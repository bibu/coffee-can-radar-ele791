#ifndef ADC_THREAD_H
#define ADC_THREAD_H

#define _GNU_SOURCE
#include <malloc.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <ctype.h>
#include <limits.h>
#include <time.h>
#include <locale.h>
#include <alsa/asoundlib.h>
#include <assert.h>
#include <termios.h>
#include <sys/poll.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <sys/signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <endian.h>
#include "gettext.h"
#include "formats.h"
#include "version.h"
#include "aconfig.h"
#include <pthread.h>

#ifndef LLONG_MAX
#define LLONG_MAX    9223372036854775807LL
#endif

#ifndef LONG_MAX
#define LONG_MAX 2147483647
#endif

#ifndef le16toh
#include <asm/byteorder.h>
#define le16toh(x) __le16_to_cpu(x)
#define be16toh(x) __be16_to_cpu(x)
#define le32toh(x) __le32_to_cpu(x)
#define be32toh(x) __be32_to_cpu(x)
#endif

#define DEFAULT_FORMAT		SND_PCM_FORMAT_U8
#define DEFAULT_SPEED 		8000

#define FORMAT_DEFAULT		-1
#define FORMAT_RAW		0
#define FORMAT_WAVE		1

#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 95)
#define error(...) do {\
	fprintf(stderr, "%s: %s:%d: ", command, __FUNCTION__, __LINE__); \
	fprintf(stderr, __VA_ARGS__); \
	putc('\n', stderr); \
} while (0)
#else
#define error(args...) do {\
	fprintf(stderr, "%s: %s:%d: ", command, __FUNCTION__, __LINE__); \
	fprintf(stderr, ##args); \
	putc('\n', stderr); \
} while (0)
#endif	

typedef struct {
	signed short * sync_ch_buffer0;
	signed short * recv_ch_buffer0;
	signed short * sync_ch_buffer1;
	signed short * recv_ch_buffer1;
	int * output_data_ready_flag;
	int * output_data_buf_num;
	int * output_num_samples;
	pthread_mutex_t *output_data_mutex;
	pthread_cond_t *output_data_cond;
} AdcThreadInfo_s;

void *adc_thread( void *ptr );

void device_list(void);

void pcm_list(void);

/*
 *	Subroutine to clean up before exit.
 */
void prg_exit(int code);

void show_available_sample_formats(snd_pcm_hw_params_t* params);

void set_params(void);

void do_pause(void);


#ifndef timersub
#define	timersub(a, b, result) \
do { \
	(result)->tv_sec = (a)->tv_sec - (b)->tv_sec; \
	(result)->tv_usec = (a)->tv_usec - (b)->tv_usec; \
	if ((result)->tv_usec < 0) { \
		--(result)->tv_sec; \
		(result)->tv_usec += 1000000; \
	} \
} while (0)
#endif

#ifndef timermsub
#define	timermsub(a, b, result) \
do { \
	(result)->tv_sec = (a)->tv_sec - (b)->tv_sec; \
	(result)->tv_nsec = (a)->tv_nsec - (b)->tv_nsec; \
	if ((result)->tv_nsec < 0) { \
		--(result)->tv_sec; \
		(result)->tv_nsec += 1000000000L; \
	} \
} while (0)
#endif

/* I/O error handler */
void xrun(void);

/* I/O suspend handler */
void suspend(void);


/*
 *  read function
 */

ssize_t pcm_read(u_char *data, size_t rcount);

ssize_t pcm_readv(u_char *sync_ch, u_char *recv_ch, size_t rcount);

/* setting the globals for playing raw data */
void init_raw_data(void);

void header();

void capture(AdcThreadInfo_s * info);

void capturev_go(int* fds, unsigned int channels, int count, int rtype, char **names);

void capturev(char **names, unsigned int count);

#endif


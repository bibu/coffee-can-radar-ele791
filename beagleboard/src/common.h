
#ifndef COMMON_H
#define COMMON_H

void writeToFile(char * fileName, u_char * buf, size_t numBytes);
 
#define MAX_FREQ_BINS_PER_PULSE 1024

#ifndef _DEBUG_
#define SAMPLES_TO_REQUEST 1024*32
#define MAX_PULSES_PER_DWELL 16
#else
#define SAMPLES_TO_REQUEST 1024*8
#define MAX_PULSES_PER_DWELL 4
#endif


#define MAX_SAMPLES_PER_PULSE 1024
#define C_NUM_FFT_BINS 1024



#endif


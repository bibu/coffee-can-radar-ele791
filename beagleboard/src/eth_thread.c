#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h> 
//#include <complex.h>
#include <fftw3.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "eth_thread.h"
#include "common.h"

void *eth_thread( void *ptr)
{
#ifdef _FILEIO_
   int written = 0;
   char filename[256];
#endif

   int sockfd;
   struct sockaddr_in server_addr;
   int slen = sizeof(server_addr);
   int foo;
   int data_size;
   int idx_pulse;
   EthThreadInfo_s * info = (EthThreadInfo_s *)ptr;
   int buf_size = 32768;
   
   fftwf_complex data[MAX_PULSES_PER_DWELL][MAX_SAMPLES_PER_PULSE];
   int idx_sample;
   

   //Setup socket
   sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
   if(sockfd < 0)
   {
      fprintf(stderr, "Failed getting socket!\n");
      exit(1);
   }

   setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &buf_size, sizeof(int));

   memset(&server_addr, 0, sizeof(server_addr));
   server_addr.sin_family = AF_INET;
   server_addr.sin_port = htons(C_PORT);
   foo = inet_aton(C_SERVER_ADDR, &server_addr.sin_addr);

   if(foo == 0)
   {
      fprintf(stderr, "inet_aton() failed\n");
      exit(1);
   }

   while(1)
   {
      pthread_mutex_lock(info->input_data_mutex);

		// wait on fft data
      while(*(info->input_data_ready_flag) == 0)
      {
         pthread_cond_wait(info->input_data_cond, info->input_data_mutex);
      }
      //printf("Eth lock!\n");

      data_size = sizeof(fftwf_complex)*C_RANGE_SAMPLES_TO_SEND;
      
#ifdef _FILEIO_
      // write to file for debugging
      if(!written)
      {
	       written=1;
          for (idx_pulse = 0; idx_pulse < MAX_PULSES_PER_DWELL; idx_pulse++)
          {
             sprintf(filename, "Eth%d.dat", idx_pulse);
             writeToFile(filename,(void *)info->fft_result[idx_pulse], data_size);
          }
      }
#endif     

/*       //Make some fake data
      for(idx_pulse = 0; idx_pulse < MAX_PULSES_PER_DWELL; idx_pulse++)
      {
         for(idx_sample = 0; idx_sample < MAX_SAMPLES_PER_PULSE; idx_sample++)
         {
            info->fft_result[idx_pulse][idx_sample] = (idx_pulse+1)-(idx_pulse+1)*_Complex_I;
         }      
      } */
      
      //Make some fake data
      for(idx_pulse = 0; idx_pulse < MAX_PULSES_PER_DWELL; idx_pulse++)
      {
         for(idx_sample = 0; idx_sample < C_NUM_FFT_BINS; idx_sample++)
         {
            //data[idx_pulse][idx_sample] = info->fft_result[idx_pulse][idx_sample][0] + info->fft_result[idx_pulse][idx_sample][1]*_Complex_I;
            data[idx_pulse][idx_sample][0] = info->fft_result[idx_pulse][idx_sample][0];
            data[idx_pulse][idx_sample][1] = info->fft_result[idx_pulse][idx_sample][1];
         }      
      }      

      // send out data over Ethernet
      for(idx_pulse = 0; idx_pulse < MAX_PULSES_PER_DWELL; idx_pulse++)
      {
         if(0)
         {
            foo = sendto(sockfd, (void *)info->fft_result[idx_pulse], data_size, 0, (struct sockaddr*)&server_addr, slen);
         }
         else
         {
            //printf("Eth send!\n");
            foo = sendto(sockfd, (void *)data[idx_pulse], data_size, 0, (struct sockaddr*)&server_addr, slen);
         }
         
         if(foo == -1)
         {
            fprintf(stderr, "sendto() failed: %s\n", strerror(errno));
            exit(1);
         }
      }
      
      // release mutex when done
      // printf("Eth unlock!\n");
      *(info->input_data_ready_flag) = 0;
      pthread_mutex_unlock(info->input_data_mutex);
   }
}

#ifndef FFT_THREAD_H
#define FFT_THREAD_H

#include <pthread.h>
#include <fftw3.h>

typedef struct {
	float ** pulse_buffer;
	fftwf_complex ** fft_result;
	pthread_mutex_t *input_data_mutex;
	pthread_cond_t *input_data_cond;
	int * input_data_ready_flag;
	pthread_mutex_t *output_data_mutex;
	pthread_cond_t *output_data_cond;
   int * output_data_ready_flag;
} FftThreadInfo_s;

void *fft_thread( void *ptr );

#endif


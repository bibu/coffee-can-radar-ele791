This is a test for FFTW using float libs.

It shows how to index into a 2D array, pulses[][]
where the first dimension is pulses and the second
dimension is samples for that pulse.

Compile with gcc -o main main.c -lfftw3f


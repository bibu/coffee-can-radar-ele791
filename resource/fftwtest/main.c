#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

// build with gcc -o main main.c -lfftw3f
// might need to run "sudo apt-get install libfftw3-dev fftw3"


/*******************************************************************************
 * Writes a chunk of bytes to a file (used for debug)
 ******************************************************************************/
void writeToFile(char * fileName, u_char * buf, size_t numBytes)
{
	FILE * outfp = fopen(fileName,"w+");
	if (outfp)
	{
		fwrite(buf, sizeof(u_char), numBytes, outfp);
		fclose(outfp); 
	}
}

int main(int argc, char * argv[])
{
	int i;
	fftwf_complex in[32][1024];
	int n = 1024;
	fftwf_complex out[32][1024];
	fftwf_plan plan_forward;
	unsigned int seed = 123456789;
	int j;
	char filename[32];

	// Alternative option for creating input
	//in = fftwf_malloc ( sizeof ( fftwf_complex ) * n );

	srand ( seed );

	for (j = 0; j < 32; j++)
	{
		for ( i = 0; i < n; i++ )
		{
			// frequency increases on each "pulse"
			in[j][i][0] = sin (2*3.141593 * i * j *10000 );
		   in[j][i][1] = 0;
		}
	}
		
	for (j = 0; j < 32; j++)
	{
	  	printf("FFT pulse %d!\n", j);
		plan_forward = fftwf_plan_dft_1d ( n, in[j], out[j], FFTW_FORWARD, FFTW_ESTIMATE );

		fftwf_execute ( plan_forward );

		sprintf(filename, "test%d.dat", j);
		writeToFile(filename, (u_char *)out[j], 1024 * sizeof(fftwf_complex));
	}

}

/*
* Nick Bedbury
*
* Reads wave file, computes FFT
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <sndfile.h>
#include <fftw3.h>

#define	BLOCK_SIZE 512

static void
print_usage (char *progname)
{	printf ("\nUsage : %s <input file> <output file>\n", progname) ;
	puts ("\n"
		"    Where the output file will contain a line for each frame\n"
		"    and a column for each channel.\n"
		) ;

} /* print_usage */

static void
convert_to_freq (SNDFILE * infile, FILE * outfile, int channels)
{	float buf [channels*BLOCK_SIZE] ;
	int k, m, index, channel, readcount ;

	fftwf_complex *in, *out;
    fftwf_plan p;

    in = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * BLOCK_SIZE);
    out = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * BLOCK_SIZE);

    p = fftwf_plan_dft_1d(BLOCK_SIZE, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    
	printf("%d channels\n", channels);

	while ((readcount = sf_readf_float (infile, buf, BLOCK_SIZE)) > 0)
	{	
		for (k = 0 ; k < readcount ; k++)
		{	

			in[k][0] = buf[k*channels + 1];
			in[k][1] = 0.0;

		}

	}

	fftwf_execute(p); /* repeat as needed */
			 
	fftwf_destroy_plan(p);

	for (index = 0; index < BLOCK_SIZE; index++)
	{
		fprintf(outfile, "%010.12f,%010.12f\n", out[index][0], out[index][1]);
	}


	/*do
	{	
		readcount = sf_readf_float (infile, buf[0], BLOCK_SIZE);
		if (readcount > 0)
		{		
			readcount = sf_readf_float (infile, buf[1], BLOCK_SIZE);
			if (readcount > 0)
			{
          		
			}
			else
			{	
				printf("! failed to read channel 2\n");
			}
		}
		else
		{
			printf("! failed to read channel 1\n");
		}


	} while (readcount > 0) ;*/

    fftwf_free(in); 
	fftwf_free(out);


	return ;
} /* convert_to_text */

int
main (int argc, char * argv [])
{	char 		*progname, *infilename, *outfilename ;
	SNDFILE	 	*infile = NULL ;
	FILE		*outfile = NULL ;
	SF_INFO	 	sfinfo ;

	progname = strrchr (argv [0], '/') ;
	progname = progname ? progname + 1 : argv [0] ;

	if (argc != 3)
	{	print_usage (progname) ;
		return 1 ;
		} ;

	infilename = argv [1] ;
	outfilename = argv [2] ;

	if (strcmp (infilename, outfilename) == 0)
	{	printf ("Error : Input and output filenames are the same.\n\n") ;
		print_usage (progname) ;
		return 1 ;
		} ;

	if (infilename [0] == '-')
	{	printf ("Error : Input filename (%s) looks like an option.\n\n", infilename) ;
		print_usage (progname) ;
		return 1 ;
		} ;

	if (outfilename [0] == '-')
	{	printf ("Error : Output filename (%s) looks like an option.\n\n", outfilename) ;
		print_usage (progname) ;
		return 1 ;
		} ;

	if ((infile = sf_open (infilename, SFM_READ, &sfinfo)) == NULL)
	{	printf ("Not able to open input file %s.\n", infilename) ;
		puts (sf_strerror (NULL)) ;
		return 1 ;
		} ;

	/* Open the output file. */
	if ((outfile = fopen (outfilename, "w")) == NULL)
	{	printf ("Not able to open output file %s : %s\n", outfilename, sf_strerror (NULL)) ;
		return 1 ;
		} ;

	fprintf (outfile, "# Converted from file %s.\n", infilename) ;
	fprintf (outfile, "# Channels %d, Sample rate %d\n", sfinfo.channels, sfinfo.samplerate) ;

	convert_to_freq (infile, outfile, sfinfo.channels) ;

	sf_close (infile) ;
	fclose (outfile) ;

	return 0 ;
} /* main */


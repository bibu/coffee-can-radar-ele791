#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include <user/include/fftw3.h>
#include <string.h>
 
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <unistd.h>

#include "bb_eth_emulator.h"

int main(void)
{
   int sockfd;
   struct sockaddr_in server_addr;
   int slen = sizeof(server_addr);
   char buf[C_BUFLEN];
   int foo;
   float data[MAX_PULSES_PER_DWELL][MAX_SAMPLES_PER_PULSE][2];
   int data_size;

   //Setup socket
   sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
   if(sockfd < 0)
   {
      fprintf(stderr, "Failed getting socket!\n");
      exit(1);
   }

   memset(&server_addr, 0, sizeof(server_addr));
   server_addr.sin_family = AF_INET;
   server_addr.sin_port = htons(C_PORT);
   foo = inet_aton(C_SERVER_ADDR, &server_addr.sin_addr);

   if(foo == 0)
   {
      fprintf(stderr, "inet_aton() failed\n");
      exit(1);
   }
   
   //Make some fake data
   for(int idx_pulse = 0; idx_pulse < MAX_PULSES_PER_DWELL; idx_pulse++)
   {
      for(int idx_sample = 0; idx_sample < MAX_SAMPLES_PER_PULSE; idx_sample++)
      {
         data[idx_pulse][idx_sample][0] = (idx_pulse+1);
         data[idx_pulse][idx_sample][1] = -(idx_pulse+1);
      }      
   }

   //Keep sending data to PC
   while(1)
   {
      if(1) //Send data
      {
         data_size = sizeof(float)*2*MAX_SAMPLES_PER_PULSE;
         
         for(int idx_pulse = 0; idx_pulse < MAX_PULSES_PER_DWELL; idx_pulse++)
         {
            foo = sendto(sockfd, (void *)data[idx_pulse], data_size, 0, (struct sockaddr*)&server_addr, slen);
         
            if(foo == -1)
            {
               fprintf(stderr, "sendto() failed\n");
               exit(1);
            }
         }
         
         usleep(500000);
      }
      else //Send user entered messages
      {
         printf("\nEnter data to send: ");
         scanf("%[^\n]",buf);
         getchar();
         
         if(strcmp(buf,"q") == 0)
         {
            exit(0);
         }

         foo = sendto(sockfd, buf, C_BUFLEN, 0, (struct sockaddr*)&server_addr, slen);

         if(foo == -1)
         {
            fprintf(stderr, "sendto() failed\n");
            exit(1);
         }
      }
   }
}

import numpy
from Parameters import Parameters as PARAM
import Plot_Radar_Data
import socket
import struct
import time

#Setup receive socket
rx_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
rx_socket.bind((PARAM.pc_ip, PARAM.radar_to_pc_port))
rx_socket.settimeout(60*10)
rx_socket.setsockopt(socket.SOL_SOCKET,socket.SO_RCVBUF,2**19)

#Setup plot
plot_obj = Plot_Radar_Data.Plot()

#Setup file output
pulse_cnt = -1;

#Init vairables
got_data = 0
num_pulses_rx = 0
old_time = 0
data = numpy.zeros((PARAM.num_pulses_per_dwell,PARAM.num_rc),numpy.complex64)
data = data + 0j
goo = numpy.zeros((PARAM.num_pulses_per_dwell,PARAM.num_rc))
last_data = numpy.zeros((1,PARAM.num_rc),numpy.complex64)
last_data = last_data + 0j

while True:

    #Wait to get some data
    try:
        rx_eth_data, addr = rx_socket.recvfrom(PARAM.rx_buffer_size)
        got_data = 1
        pulse_cnt = pulse_cnt + 1
    
    except socket.timeout:  
        foo = raw_input('Socket timed out. Keep listening [y|n]:')    
        
        if(foo != "y"):
            break     
        
    if(got_data == 1):
        got_data = 0
        
        #Convert string to float    
        foo = struct.unpack('f'*(len(rx_eth_data)/struct.calcsize('f')), rx_eth_data)
        foo = numpy.asarray(foo) 

        #Write raw data to file
        if(PARAM.write_pulses_to_file == 1): 
           if(pulse_cnt < PARAM.num_pulses_per_dwell):
              file_name = "python_%d.bin" % pulse_cnt
              file_handle = open(file_name, 'w')
              foo.tofile(file_handle)
              file_handle.close()
        
        #Parse into complex and assemble a dwell
        data[num_pulses_rx,] = 1j*foo[1::2] + foo[0::2] 
        num_pulses_rx = num_pulses_rx + 1
                    
        #pdb.set_trace()

        if(num_pulses_rx == PARAM.num_pulses_per_dwell):
            num_pulses_rx = 0
            
            #Determine if the data is bad or not
            goo = abs(data)
    
            if(goo[1,1] != 0):
                bad_data = 0
           
            else:
                bad_data = 1
        
            if(bad_data == 0):
                #Do Doppler or MTI 
                if(PARAM.processing_type == "doppler"):
                    for idx in range(0, PARAM.num_rc):
                        data[::,idx] = numpy.fft.fft(data[::,idx],n=PARAM.num_pulses_per_dwell)                            
                        data[::,idx] = numpy.fft.fftshift(data[::,idx])
                        
                elif(PARAM.processing_type == "2 pulse cancler"):
                    temp_data = data # make a copy of data before modifying it
                    for idx in range(1, PARAM.num_pulses_per_dwell):                     
                        data[idx,::] = temp_data[idx,::] - temp_data[idx-1,::]
                    data[0,::] = temp_data[0,::] - last_data[::]
                    last_data[::] = temp_data[PARAM.num_pulses_per_dwell-1,::]
    
                #Convert to magnitude
                data = abs(data)
                
                try:
                   for idx in range(0, PARAM.num_pulses_per_dwell):
                      data[idx,::] = 20*numpy.log10(data[idx,::])
                      
                except:
                   print 'Exception in log10()'
                            
                #Plot what we get
                plot_obj.update(data)
                new_time = time.time()
                print("Time delta is: %0.3f" % (new_time-old_time))  
                old_time = new_time     
            
            #Reinitialize on each loop to prevent cast error
            data = numpy.zeros((PARAM.num_pulses_per_dwell,PARAM.num_rc),numpy.complex64)
            data = data + 0j

rx_socket.close()

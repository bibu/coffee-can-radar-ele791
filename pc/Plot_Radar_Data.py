import matplotlib
import matplotlib.pyplot
import numpy 
from Parameters import Parameters as PARAM

class Plot:
    def __init__(self, I_FIG_NUM=791):
        #Setup variables        
        if(PARAM.processing_type == "doppler"):
            self.data = numpy.random.rand(PARAM.num_pulses_per_dwell,PARAM.num_rc)
        else:
            self.data = numpy.random.rand(PARAM.pulses_to_plot,PARAM.num_rc)
            
        self.pulses_plotted = 0
        
        #Setup plot
        matplotlib.pyplot.close(I_FIG_NUM)
        self.handle_fig = matplotlib.pyplot.figure(I_FIG_NUM)
        self.handle_axes = self.handle_fig.add_subplot(1,1,1)
        self.handle_img = self.handle_axes.imshow(self.data, origin='lower', aspect='auto')
        self.handle_cbar = self.handle_fig.colorbar(self.handle_img)

        if(PARAM.processing_type == "doppler"):
            matplotlib.pyplot.ylabel('Doppler Bin')
            self.handle_img.set_extent([0,PARAM.num_rc,-PARAM.num_pulses_per_dwell/2,PARAM.num_pulses_per_dwell/2-1])     
        else:          
            matplotlib.pyplot.ylabel('Pulse #')
            self.handle_img.set_extent([0,PARAM.num_rc,0,PARAM.pulses_to_plot])     
        
        matplotlib.pyplot.xlabel('Range Cell')
        matplotlib.pyplot.title('Radar Output')    

        self.handle_fig.canvas.draw()
        matplotlib.pyplot.pause(0.001)
        
    def update(self, I_DATA):
        if(PARAM.processing_type == "doppler"):
            self.data = I_DATA
        else:    
            self.data = numpy.vstack([self.data,I_DATA])
            self.data = self.data[PARAM.num_pulses_per_dwell-1::,:]
                    
        #Update the figure        
        self.handle_img.set_array(self.data)

        if(PARAM.fixed_clim == True):
            self.handle_cbar.set_clim(PARAM.clim)        
        else:
            self.handle_cbar.set_clim(self.data.max()-80, self.data.max())

        self.handle_img.changed()   
        self.handle_fig.canvas.draw()                
        matplotlib.pyplot.pause(0.001)
